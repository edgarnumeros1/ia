//codigo del problema del salto del caballo

#include <iostream>
using namespace std;

const int N = 3;
int tablero[N][N];

//desplazamientos relativos del caballo
int d[20][2] = {{1,-1},{-1,-1},{1,0},{-2,2},{2,0},{1,-1},{1,-1},{1,0},{1,-1},{1,-1},{-1,0}};

void escribirTablero()
{
	int i,j;
	
	for (i=0;i<N;i++)
	{
		for (j=0;j<N;j++)
		{
			cout<<tablero[i][j]<<"|";
        }
		cout<<endl;
	}
	cout<<endl;
}

void saltoCaballo(int i, int x, int y, bool &exito)
{
	int nx, ny;
	int k=0; //interador para controlar los desplazamientos del caballo
	exito = false;
	
	do
	{
		k++;
		nx = x + d[k-1][0];
		ny = y + d[k-1][1];
		
		//verificando si las coordenadas son correctas
		if ((nx>=0) && (nx<N) && (ny>=0) && (ny<N) && (tablero[nx][ny]==0))
		{
		    //Guardamos el movimiento
		    tablero[nx][ny] = i;
		    escribirTablero(); //mostrando como el caballo se mueve
		    
		    if(i < N*N){
		    	saltoCaballo(i+1,nx,ny,exito);
		    	
		    	if ( !exito )
				{
		    		tablero[nx][ny] = 0;
				}
			}
			else 
			{
				exito = true;
			}
		}
    }while((k<8) && !exito);
}
	

int main (){
	bool exito;
	int fila=1;
	int col=0;
	
	tablero[fila][col] = 1;
	saltoCaballo(2,fila,col,exito);
	
	if (exito){
		cout<<"Camino Encontrado"<<endl;
		escribirTablero();
	}
	else{
		cout<<"Camino No Encontrado"<<endl;
	}
	
	return 0;
}


























